/* eslint-disable no-console */
const WebSocket = require('ws')
const uuid = require('uuid')

const wsServer = new WebSocket.Server({ port: process.env.PORT || 58496 })

const respond = (ws, message) => ws.send(JSON.stringify(message))

const mock = [
  [
    'Amazing grace! How sweet the sound',
    'That saved a wretch like me!',
  ],
  [
    'I once was lost, but now am found;',
    'Was blind, but now I see.',
  ],
  [
    'Through many dangers, toils and snares,',
    'I have already come;',
  ],
  [
    '',
    '',
  ],
  // [
  //   '',
  //   '',
  // ]
].map(verse => verse.join('\r\n'))

let current = 0
const nextSlide = () => {
  current = (current + 1) % mock.length

  return {
    acn: 'fv',
    ary: [
      {
        acn: 'cs',
        uid: uuid(),
        txt: mock[current],
      },
      {
        acn: 'ns',
        uid: uuid(),
        txt: mock[(current + 1) % mock.length],
      },
      {
        acn: 'csn',
        txt: '',
      },
      {
        acn: 'nsn',
        txt: '',
      },
    ],
  }
}

const interpreter = (ws, { acn, pwd, ptl }) => {
  switch (acn) {
    case 'ath': {
      if (ptl === 610 && pwd === (process.env.PASSWORD || 'av')) {
        return respond(ws, { acn: 'ath', ath: true, err: '' })
      }
      return respond(ws, { acn: 'ath', ath: false, err: 'Wrong password' })
    }
    default:
      return null
  }
}

const onMessage = (message, ws) => {
  console.log('message:', message)
  try {
    const command = JSON.parse(message)

    interpreter(ws, command)
  } catch (error) {
    console.error(error.message)
  }
}

wsServer.on('connection', ws => {
  const interval = setInterval(() => {
    respond(ws, nextSlide())
  }, 5000)

  ws.on('close', () => clearInterval(interval))
  ws.on('message', message => onMessage(message, ws))
})
