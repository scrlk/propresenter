const gulp = require('gulp')

const autoprefixer = require('gulp-autoprefixer')
const cleanCss = require('gulp-clean-css')
const less = require('gulp-less')
const size = require('gulp-size')
const clean = require('gulp-clean')
const sourcemaps = require('gulp-sourcemaps')
const browserSync = require('browser-sync').create()
const babel = require('gulp-babel')
const concat = require('gulp-concat')

const {
  src,
  dest,
  series,
  parallel,
  watch,
} = gulp

const dist = './public'
const paths = {
  pages: [
    ['./source/index.twig', './source/index.json'],
  ],
  styles: './source/styles/*.less',
  scripts: './source/scripts/*.js',
  other: ['./source/**/*', '!source/styles/**/*', '!source/scripts/**/*'],
}

const copy = () => src(paths.other)
  .pipe(dest(dist))

const legacy = () => src('./legacy/**/*')
  .pipe(dest(`${dist}/legacy`))

const serve = () => {
  browserSync.init({
    server: {
      baseDir: dist,
    },
  })
}

const reload = done => {
  browserSync.reload()
  done()
}

const styles = () => src(paths.styles)
  .pipe(sourcemaps.init())
  .pipe(less())
  .pipe(autoprefixer({
    cascade: false,
  }))
  .pipe(cleanCss())
  .pipe(sourcemaps.write('.'))
  .pipe(size({
    showFiles: true,
  }))
  .pipe(dest(`${dist}/styles`))
  .pipe(browserSync.stream())

const scripts = () => src(paths.scripts, { sourcemaps: true })
  .pipe(babel())
  .pipe(concat('index.js'))
  .pipe(dest(`${dist}/scripts`))

const watchStyles = () => watch(paths.styles, series(styles, reload))

const watchScripts = () => watch(paths.scripts, series(scripts, reload))

const watchOther = () => watch(paths.other, series(copy, reload))

const clear = () => src(dist, {
  read: false,
  allowEmpty: true,
})
  .pipe(clean())

const build = series([
  clear,
  legacy,
  parallel([
    copy,
    styles,
    scripts,
  ]),
])

exports.build = build

exports.default = series(
  build,
  parallel([
    serve,
    watchStyles,
    watchScripts,
    watchOther,
  ]),
)
