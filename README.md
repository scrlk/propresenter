# ProPresenter remote display

Development requires NodeJS

``` sh
npm i
npm start
```

- `./mock` – dummy application simulating ProPresenter API
- `./source` – main part of the code
- `./public` – output when compiled, to run on the screen

## Enable remote access

Navigate to Preferences -> Network tab. Set and remember **IP** and **port** for network connection.

Usage in private, secured network advised.

## Prior work
- [ProPresenter-API](https://github.com/jeffmikels/ProPresenter-API) by Jeff Mikels
- [ProPresenter-Lyrics-HTML](https://github.com/anthonyeden/ProPresenter-Lyrics-HTML) by Anthony Eden
