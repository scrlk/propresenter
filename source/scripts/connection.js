/* global window, WebSocket, getConfig, setStatus, setCurrentSlide, setNextSlide,  */
/* eslint-disable no-console */

let socket

const send = message => socket.send(JSON.stringify(message))

const replaceSlideTxt = (ary, target, source) => {
  const keys = ary.map(command => command.acn)
  const commands = ary

  if (keys.includes(source) && keys.includes(target)) {
    const note = commands.find(command => command.acn === source)
    const index = commands.findIndex(command => command.acn === target)

    if (note.txt) {
      commands[index].txt = note.txt
    }
  }

  return commands
}

const interpreter = ({
  acn,
  ath,
  ary,
  err,
  txt,
  // uid,
}) => {
  const { showNext } = getConfig()
  switch (acn) {
    case 'ath': { // authorization
      if (ath && !err) {
        setStatus('Connected', 'info')
        setTimeout(() => {
          setStatus()
        }, 1500)
      }
      break
    }
    case 'fv': { // compound command
      let commands = ary
      commands = replaceSlideTxt(commands, 'cs', 'csn')
      commands = replaceSlideTxt(commands, 'ns', 'nsn')

      commands.forEach(interpreter)
      break
    }
    case 'cs': {
      setCurrentSlide(txt)
      break
    }
    case 'csn': {
      // console.log(txt) // current slide notes
      break
    }
    case 'ns': {
      setNextSlide(showNext !== false ? txt : '')
      break
    }
    case 'nsn': {
      // console.log(txt) // next slide notes
      break
    }
    // case 'sys': {
    //   setTime(txt)
    //   break
    // }
    default: {
      // console.log('unknown command', acn)
    }
  }
}

const onMessage = event => {
  const { data } = event
  try {
    const command = JSON.parse(data)

    interpreter(command)
  } catch (error) {
    console.warn('[error]', error)
    socket.close()
  }
}

function connect() {
  const { ip, port, password } = getConfig()
  const url = `ws://${ip}:${port}/stagedisplay`

  socket = new WebSocket(url)
  const reconnect = () => {
    setTimeout(() => {
      setStatus('Connection error', 'warning')
    }, 1000)

    setTimeout(connect, 5000)
  }

  setStatus('Connecting…', true)

  socket.addEventListener('open', () => {
    console.log('[open] Connection established')
    setStatus('Authenticating…', true)

    send({
      pwd: password,
      ptl: 610,
      acn: 'ath',
    })
  })

  socket.addEventListener('message', onMessage)

  socket.onclose = event => {
    if (event.wasClean) {
      console.warn(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`)
    } else {
      console.warn('[close] Connection died')
    }

    reconnect()
  }

  socket.addEventListener('error', error => {
    console.warn(`[error] ${error.message || ''}`)
  })
}

window.connect = connect
