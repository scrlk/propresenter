/* global document, window */

function setStatus(text = '', icon = '') {
  const $text = document.querySelector('#status-text')
  const $icon = document.querySelector('#status-icon')
  const $spinner = document.querySelector('#status-spinner')

  if ($text) {
    $text.textContent = text
  }

  if ($icon) {
    if (icon === 'spinner' || icon === true) {
      $icon.classList.add('hidden')
      $spinner.classList.remove('hidden')
    } else {
      $spinner.classList.add('hidden')
      $icon.classList.remove('hidden')
      $icon.setAttribute('uk-icon', `icon: ${icon}; ratio: 1.5`)
    }
  }
}

window.setStatus = setStatus
