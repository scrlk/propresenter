/* global document, initializeConfig, connect, scheduleClock */
/* eslint-disable no-console */

function ready(fn) {
  if (document.readyState !== 'loading') {
    fn()
  } else {
    document.addEventListener('DOMContentLoaded', fn)
  }
}

ready(initializeConfig)
ready(scheduleClock)
ready(connect)
