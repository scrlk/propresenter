/* global document, window, getConfig */

const showClock = visible => {
  const $clock = document.querySelector('#clock')

  if (visible) {
    $clock.classList.remove('invisible')
  } else {
    $clock.classList.add('invisible')
  }
}

const updateClock = () => {
  const $h = document.querySelector('#h')
  const $m = document.querySelector('#m')
  // const $s = document.querySelector('#s')

  const d = new Date()
  const h = `${d.getHours()}`.padStart(2, '0')
  const m = `${d.getMinutes()}`.padStart(2, '0')
  // const s = '.'.repeat(d.getSeconds())

  $h.innerHTML = h
  $m.innerHTML = m
  // $s.innerHTML = s
}

const scheduleClock = () => {
  const { showclock } = getConfig()

  if (showclock && document.querySelector('#clock')) {
    setInterval(updateClock, 250)
  }
}

window.showClock = showClock
window.scheduleClock = scheduleClock
