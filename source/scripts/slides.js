/* global document, window, showClock, getConfig */

const updateVisibility = () => {
  const { shownext } = getConfig()
  const $slides = document.querySelectorAll('#current, #next')

  if (!$slides) return null

  let visible = false

  $slides.forEach($element => {
    if (!$element.classList.contains('empty')) {
      visible = true
    }
  })

  if (!shownext && document.querySelector('#current').classList.contains('empty')) {
    visible = false
  }

  $slides.forEach($element => {
    if (visible) {
      $element.classList.remove('invisible')
    } else {
      $element.classList.add('invisible')
    }
  })

  showClock(!visible)

  return visible
}

const setSlide = (target, text) => {
  const $target = document.querySelector(target)

  if (!$target) return null

  const parsedText = text.replace(/\r?\n\r?/g, '\n')
  const splittedText = parsedText.split('\n')
  // const onlySecondHalf = splittedText.slice(splittedText.length / 2)
  const formattedText = splittedText.join('<br />')
  const slide = `<span>${formattedText}</span>`

  if (text.replace(/\s/g, '') === '') {
    $target.classList.add('empty')
  } else {
    $target.classList.remove('empty')
  }

  if (updateVisibility()) {
    $target.innerHTML = slide

    return true
  }

  return setTimeout(() => {
    $target.innerHTML = slide
  }, 1000)
}

const setCurrentSlide = txt => setSlide('#current', txt)
const setNextSlide = txt => setSlide('#next', txt)
const setTime = txt => {
  const $clock = document.querySelector('clock')

  if (!$clock) return null

  $clock.innerHTML = txt

  return txt
}

window.setCurrentSlide = setCurrentSlide
window.setNextSlide = setNextSlide
window.setTime = setTime
