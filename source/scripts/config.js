/* global document, window, localStorage, connect, loadStylesConfig */
/* eslint-disable no-console */

const defaultConfig = {
  ip: '127.0.0.1',
  port: 58496,
  password: 'av',
  color: '#ffffff',
  bgcolor: '#000000',
  clockcolor: '#aaaaaa',
  accentcolor: '#ffff00',
  font: 'Alegreya Sans SC',
  showclock: true,
  shownext: true,
  fontweight: 700,
  fontmain: 9,
  fontsecondary: 6,
  lineheight: 0.8,
  padding: 0.2,
}

function getFormValues() {
  const newConfig = {}
  document.querySelectorAll('input, select').forEach($input => {
    const name = $input.getAttribute('name')
    const type = $input.getAttribute('type')
    const { value, checked } = $input

    if (type === 'checkbox') {
      newConfig[name] = checked
      return checked
    }

    if (name && value !== '') {
      newConfig[name] = value
    }

    return value
  })

  const config = {
    ...newConfig,
  }

  return config
}

function setFormValues(loadedConfig) {
  const config = {
    ...defaultConfig,
    ...loadedConfig,
  }

  document.querySelectorAll('input, select').forEach($input => {
    const name = $input.getAttribute('name')
    const type = $input.getAttribute('type')
    const { checked } = $input
    const value = config[name]
    const defaultValue = defaultConfig[name]

    if (name && (value !== null && value !== undefined)) {
      if (type !== 'color') {
        $input.setAttribute('placeholder', defaultValue)
      } else {
        $input.value = defaultValue // eslint-disable-line no-param-reassign
      }

      if (type === 'checkbox' && checked !== value) {
        $input.checked = value // eslint-disable-line no-param-reassign
      } else if (value !== defaultValue) {
        $input.value = value // eslint-disable-line no-param-reassign
      }
    }
  })
}

function loadConfig() {
  const configString = localStorage.getItem('CONFIG')
  return configString ? JSON.parse(configString) : {}
}

function saveConfig(config) {
  return localStorage.setItem('CONFIG', JSON.stringify(config))
}

function getConfig() {
  return {
    ...defaultConfig,
    ...loadConfig(),
  }
}

function onConfigSave(event) {
  event.preventDefault()
  saveConfig(getFormValues())
  connect()
}

function onGenerateUrl(event) {
  event.preventDefault()
  window.location = `./#${encodeURI(JSON.stringify(getFormValues()))}`
}

function saveFromUrlHash() {
  if (window.location.hash) {
    saveConfig(JSON.parse(decodeURI(window.location.hash).slice(1)))

    setTimeout(() => {
      window.location = './'
    }, 10000)
  }
}

function initializeConfig() {
  const $saveButton = document.querySelector('#save_config')
  const $urlConfigButton = document.querySelector('#generate_url')

  if ($saveButton) {
    setFormValues(loadConfig())

    $saveButton.addEventListener('click', onConfigSave)
    $urlConfigButton.addEventListener('click', onGenerateUrl)
  } else {
    saveFromUrlHash()
    loadStylesConfig()
  }
}

window.getConfig = getConfig
window.initializeConfig = initializeConfig
