/* global document, window, getConfig */

function loadStylesConfig() {
  const config = getConfig()
  const $body = document.querySelector('body')
  const $head = document.querySelector('head')
  const $current = document.querySelector('#current')
  const $next = document.querySelector('#next')
  const $clock = document.querySelector('#clock')
  const $slides = document.querySelectorAll('.slides')

  $body.style.backgroundColor = config.bgcolor
  $body.style.backgroundImage = `url(${config.logo})`
  $body.style.backgroundPositionX = config.logohorizontal
  $body.style.backgroundPositionY = config.logovertical
  $body.style.color = config.color
  $next.style.color = config.accentcolor
  $clock.style.color = config.clockcolor
  $clock.style.display = config.showclock ? 'block' : 'none'
  $next.style.display = config.shownext ? 'block' : 'none'

  $current.style.fontSize = `${config.fontmain}vw`
  $next.style.fontSize = `${config.fontsecondary}vw`

  $slides.forEach($slide => {
    $slide.style.lineHeight = config.lineheight // eslint-disable-line no-param-reassign
    $slide.style.padding = `${config.padding}em` // eslint-disable-line no-param-reassign
    $slide.style.textAlign = config.texthorizontal // eslint-disable-line no-param-reassign
  })
  $body.classList.add(`slides-vertical-${config.texthorizontal}`)

  $body.style.fontFamily = `${config.font}, sans-serif`
  $body.style.fontWeight = config.fontweight

  const googleFonts = document.createElement('link')
  googleFonts.setAttribute('rel', 'stylesheet')
  googleFonts.setAttribute('async', true)
  googleFonts.setAttribute('href', `https://fonts.googleapis.com/css?family=${
    config.font.replace(/ /g, '+')
  }:${
    config.fontweight
  }&display=swap&subset=latin-ext`)

  $head.append(googleFonts)
}

window.loadStylesConfig = loadStylesConfig
